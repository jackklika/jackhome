+++
title = "Work"
slug = "work"
+++

## Unified for Progress [🔗](https://joinunified.us)
In 2022, the most popular social media platforms focus on user attention instead of user action. Without action, it's impossible to make political change. Unified is a mobile application for organizing political action by providing a rich toolset for organizers and volunteers, empowering users to do actions like seeing if contacts are registered to vote or looking up local representatives.

My work on this has been to lead the backend engineering, developing an application API for the mobile app to interact with, and working on a data backend and library which can perform parallel data processing. Using FastAPI has made it easy to deploy quickly across multiple environments.

## Foxconn Industrial AI Institute Academy [🔗](https://iaiinstitute.com/index)
After transitioning from an assistant role to a devops role at Foxconn Industrial Internet's Industrial AI Institute, I worked on the kubernetes architecture and deployment for their education platform. The platform allows users to buy credits, and spend those credits on jupyter instances so they could solve industrial problems by writing machine learning code which ran against our proprietary manufacturing data.

Behind this scenes, I worked on creating ansible scripts to spin up the kubernetes cluster on EC2, integrating the cluster with the  backend so it could schedule these notebook instances to run on the cluster, wrote a python program to autoscale the cluster by starting and stopping EC2 instances that made up the cluster, and finally deployed a monitoring stack using prometheus to collect cluster and application stats, loki to gather up our logs, and grafana to visualize these metrics.

## The Labyrinth [🔗](https://gitlab.com/uwmcomputersociety/labyrinth-site)
In 2018, I created a coding challenge website for [UWM IEEE-CS](http://uwmcomputer.club/) members to test their programming ability. Instead of using email signup, users are identified by pubkey and answers are submitted using ssh. This was mostly written in internet cafes as I travelled around China.

## Puppetmonitor [🔗](https://gitlab.com/jackklika/puppetmonitor)
For CGCA and LSC, I made a page to monitor our virtual servers. It was a simple project, but taught me about Go and infrastructure monitoring.

## Chinese Culture Class Presentations
During my time at Nanjing University, I took part in a cultural class which required us to present ten presentations over the course of the semester. I covered topics such as BGP hijacking, orthodox Marxist student organizations, Foxconn's project in Wisconsin, and open source software.

## Wikivoyage [🔗](https://en.wikivoyage.org/wiki/User:Jackklika)
When I travel, I find wikivoyage to be a valuable resource to learn more about a place. To contribute back to the community, I've contributed to some pages.

