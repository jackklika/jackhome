+++
title = "About"
slug = "about"
+++

On the the homepage you can find my linkedin, github, and gitlab. Most of my activity takes place on private repos, so contact me if you're interested in understanding what I've been working on.

I am the founder of [Silversense Software Services](http://silversense.org/).

This page was generated with [Hugo](gohugo.io) and styled with the [hugo-coder](https://github.com/luizdepra/hugo-coder/) theme, with some personal modifications.
